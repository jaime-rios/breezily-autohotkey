# Breezily for Windows

This [AutoHotkey](https://www.autohotkey.com/) app helps make Windows a breeze to navigate. It's loosely based off [macOS keyboard shortcuts](https://support.apple.com/en-us/HT201236).

Breezily has been tested with an English (US) keyboard layout, but should work under different layouts too. [SharpKeys](https://github.com/randyrants/sharpkeys) is required to use Breezily.

Good Stuff To Know:
- **What is AutoHotkey?** - An automation scription language for Windows.
- **What are Hotkeys?** - Automation commands programmed with [AutoHotkey](http://autohotkey.com/).
- **What is Hotclicking?** - Using a hotkey command to mouse click.
- **What is a Hotcursor** - A hotkey command to move the mouse pointer/cursor.

## Installing AutoHotkey & Breezily

1) Download and install [AutoHotkey](https://www.autohotkey.com/).
2) Download the latest [release](https://gitlab.com/jaimejrios/breezily-autohotkey/-/releases) of Breezily.
3) Add your downloaded `Breezily.exe` file to the `/Local Disk (C:)/Program Files/AutoHotkey/` directory.

## Setting Up SharpKeys & Breezily

Let's set up Breezily using Window's Task Scheduler app (so that it starts at log on).

**1)** Press the `⊞ Windows` and `R` keys together, and enter `taskschd.msc` on the Run menu.

![Step 1: Open task scheduler application](img/setup-task/1-open-task-scheduler-app.JPG)

**2)** Next, click the `Create Task` option on the right side of the Task Scheduler app (under the `Actions` menu).

![Step 2: Create task from task scheduler menu](img/setup-task/2-create-task-from-menu.JPG)

**3)** Enter in a name for your task (i.e. `Run Breezily`).

**4)** Select the option `Run only when user is logged on`.

**5)** Check the box `Run with highest privileges` (this enables Breezily commands to run inside admin-elevated apps like Git Bash).

**6)** Click on the `Configure For` drop-down menu and select `Windows 10` (don't click `OK` just yet.

![Steps 3-6: Editing general options when creating a task](img/setup-task/3-6-edit-general-options.JPG)

**7)** Next, click on the `Triggers` tab, then click `New...`.

![Step 7: Adding a new trigger when creating a task](img/setup-task/7-create-new-trigger.JPG)

**8)** Afterwards, click the `Begin the task:` drop-down menu and select `At log on`, then click `OK`.

![Step 8: Setting up a new trigger at log on](img/setup-task/8-set-up-new-trigger-at-log-on.JPG)

**9)** Click the `Actions` tab (near the top
of the `Create Task` menu), then click `New`.

![Step 9: Creating a new action when creating a task](img/setup-task/9-create-new-action.JPG)

**10)** Then click the `Action:` drop-down menu and select `Start a program`.

**11)** Now click `Browse...` and select the `Breezily.exe` file at its current file location, then click `OK`.

![Steps 9-11: Adding a new action when creating a task](img/setup-task/10-11-new-action-options.JPG)

**12)** After that, click the `Conditions` tab, and uncheck both options under the `Power` category.

![Step 12: Editing the task's conditions](img/setup-task/12-set-up-conditions.JPG)

**13)** Next, click the `Settings` tab, and uncheck the `Stop the task if it runs longer than:` option, then click `OK`.

![Step 13: Editing the task's settings](img/setup-task/13-edit-settings-options.JPG)

**Almost Done!** Let's install SharpKeys so we can remap the `Caps Lock` key. Then Breezily will be ready to go!

**1)** Download the latest version of [SharpKeys](https://github.com/randyrants/sharpkeys/releases/) and select the `.msi` file installer.

**2)** In your Downloads folder, double-click on the SharpKeys `.msi` installer.

**3)** Follow the steps to install SharpKeys on your system.

**4)** Press the `⊞ Windows` key and type `SharpKeys` to locate the application. Then click on `SharpKeys` to open the app.

**5)** After SharpKeys is open, click `Add` to add a new key mapping.

![Step 5: Adding a new key mapping within SharpKeys](img/setup-sharpkeys/5-add-key-mappings.JPG)

**6)** Under the `Add New Key Mapping` menu, select the `Caps Lock` key on the left menu and `F23` on the right menu, then click `OK`.

![Step 6: Mapping Caps Lock key to F23 key using SharpKeys](img/setup-sharpkeys/6-map-caps-lock-to-f23.png)

**7)** After that, click on `Write to Registry`.

![Step 6: Writing new key mappings to registry within SharpKeys](img/setup-sharpkeys/7-write-key-mappings-to-registry.png)

**7)** Restart your PC so that the SharpKeys modifications can take effect.

**Now to make sure Breezily runs smoothly when snapping windows**, follow these two quick steps...

**1)** Press the `⊞ Windows` key and type `Snap Settings`, then press `Enter`.

**2)** Leave the `Snap windows` option switched on, as well as the `When I snap a window, automatically size it to fill available space` option. Uncheck all other options.

![Step 2: Modify Snap Settings within Windows Multitasking Settings](img/snap-settings/2-snap-settings.JPG)

**3)** You're all done! Enjoy Breezily :)


# Breezily Keyboard Shortcuts

Please note that the shortcut behvaior could vary depending on the app in use.

## Essential Shortcuts
- **Caps Lock-Space**: Press the `⊞ Windows` key.
- **Caps Lock-Q**: Press the `Escape` key.
- **Caps Lock-Z**: Undo the previous command. You can then press **Caps Lock-X** to Redo, reversing the undo command. In some apps, you can undo and redo multiple commands.
- **Caps Lock-C**: Copy the selected item to the Clipboard. This also works for files in the File Explorer.
- **Caps Lock-I**: Cut the selected item and copy it to the Clipboard.
- **Caps Lock-V**: Paste the contents of the Clipboard into the current document/app. This also works for files in the File Explorer.
- **Caps Lock-G**: Find items in a document or open the Find tool.
- **Caps Lock-S**: Save the current document/item.
- **Left Shift-Space**: Rename a selected file/item.
- **Right Shift-Space**:  Create a new folder in File Explorer.
- **Caps Lock-9**: Insert a unicode bullet character.
- **Caps Lock-Enter**: Press the control key and enter key simultaneously.
- **Right Alt-Q**: Quit an application.

## Document Shortcuts

- **Caps Lock-D**: Delete the character to the right of the insertion point.
- **Caps Lock-H**: Delete the character to the left of the insertion point.
- **Caps Lock-F**: Move one character forward.
- **Caps Lock-B**: Move one character backward.
- **Caps Lock-P**: Move up one line.
- **Caps Lock-N**: Move down one line.
- **Caps Lock-O**: Scroll up on the current page.
- **Caps Lock-L**: Scroll down on the current page.
- **Caps Lock-Y**: Highlight/select all items.
- **Caps Lock-A**: Move to the beginning of the line/paragraph.
- **Caps Lock-E**:  Move to the end of the line/paragraph.
- **Caps Lock-K**: Delete text between the insertion point and the end of line/paragraph.
- **Caps Lock-Semicolon (;)**: Extend text selection one character to the left.
- **Caps Lock-Apostrophe (')**: Extend text selection one character to the right.
- **Caps Lock-Left Curly Brace ({)**: Extend text selection to beginning of the current word, then to the beginning of the following word if pressed again.
- **Caps Lock-Right Curly Brace (})**: Extend text selection to end of the current word, then to the end of the following word if pressed again.
- **Caps Lock-Backspace**: Select the text between the insertion point and the beginning of the current line.
- **Caps Lock-Backslash (\\)**: Select the text between the insertion point and the end of the current line.
- **Caps Lock-U**: Extend text selection to the nearest character at the same horizontal location on the line above.
- **Caps Lock-J**: Extend text selection to the nearest character at the same horizontal location on the line below.

## Desktop Shortcuts

- **Caps Lock-Right Control**: Open the Task view panel.
- **Caps Lock-8**: Add a new virtual desktop.
- **Caps Lock-5**: Switch between the virtual desktops you’ve created on the right.
- **Caps Lock-7**: Switch between the virtual desktops you’ve created on the left.
- **Caps Lock-6**: Close the active virtual desktop.

## Window Shortcuts

- **Caps Lock-4**: Open a new window.
- **Caps Lock-W**: Close the active window.
- **Caps Lock-M**: Minimize the current window to the task bar.
- **Caps Lock-Less-Than Sign (<)**: Maximize the current window to the left side of the screen.
- **Caps Lock-Greater-Than Sign (>)**: Maximize the current window to the right side of the screen.
- **Caps Lock-Forward Slash (/)**: Maximize the current window.

## Web Browser Shortcuts

- **Caps Lock-R**: Reload the current page.
- **Caps Lock-T**: Open a new tab, and move to it.
- **Caps Lock-Tab**: Move to the next open tab.
- **Caps Lock-Right Shift**: Move to the previous open tab
- **Caps Lock-Backtick (`)**: Reopen the last closed tab, and move to it.
- **Caps Lock-1**: Open the previous page from your browsing history in the current tab.
- **Caps Lock-2**: Open the next page from your browsing history in the current tab.
- **Caps Lock-3**: Move to the address bar.
- **Caps Lock-0**: Return contents on the page to its default size.
- **Caps Lock-Minus sign (-)**: Decrease the size of the current page.
- **Caps Lock-Plus sign (+)**: Increase the size of the current page.

## Mouse Cursor Shortcuts

- **Alt-C**: Click the left mouse button.
- **Alt-V**: Click the right mouse button.
- **Alt-W**: Move the mouse cursor up.
- **Alt-A**: Move the mouse cursor left.
- **Alt-S**: Move the mouse cursor down.
- **Alt-D**: Move the mouse cursor right.
- **Alt-Shift-W**: Move the mouse cursor up at double the distance.
- **Alt-Shift-A**: Move the mouse cursor left at double the distance.
- **Alt-Shift-S**: Move the mouse cursor down at double the distance.
- **Alt-Shift-D**: Move the mouse cursor right at double the distance.
- **Alt-Control-W**: Move the mouse cursor up at half the distance.
- **Alt-Control-A**: Move the mouse cursor left at half the distance.
- **Alt-Control-S**: Move the mouse cursor left at half the distance.
- **Alt-Control-D**: Move the mouse cursor left at half the distance.
- **Alt-1**: Move the mouse cursor to the top left of the screen.
- **Alt-2**: Move the mouse cursor to the top center of the screen.
- **Alt-3**: Move the mouse cursor to the top right of the screen.
- **Alt-4**: Move the mouse cursor to the middle left of the screen.
- **Alt-5**: Move the mouse cursor to the center of the screen.
- **Alt-6**: Move the mouse cursor to the middle right of the screen.
- **Alt-7**: Move the mouse cursor to the bottom left of the screen.
- **Alt-8**: Move the mouse cursor to the bottom center of the screen.
- **Alt-9**: Move the mouse cursor to the bottom right of the screen.
- **Alt-0**: Move the mouse cursor to the center of the current open window.

## AutoHotkey Script Guide

| Symbol   | Description        |
|----------|--------------------|
| `;`      | Comment            |
| `!`      | `Alt` key          |
| `^`      | `Ctrl` key         |
| `+`      | `Shift` key        |
| `#`      | `⊞ Windows` key    |
| `vkXX`   | Virtual Key        |
| `U+nnnn` | Unicode Character  |

### Credits
---
- The [AutoHotkey Foundation](https://www.autohotkey.com/) and [Community](https://www.autohotkey.com/boards/).
- [Randy](https://github.com/stroebjo/autohotkey-windows-mac-keyboard) - Creator of [SharpKeys](https://github.com/randyrants/sharpkeys).
- [Jonathan Ströbele](https://github.com/stroebjo) - Breezily was inspired from his [work](https://github.com/stroebjo/autohotkey-windows-mac-keyboard).
